import java.util.Scanner;

public class TypeOfArray {

        public static int arrayType(int[] arr) {
            int odd = 0;
            int even=0;
            for (int i:arr) {
                if (i % 2 == 0) {
                    even += 1;
                } else odd += 1;
            }
            if (odd == 0) {
                return 1;
            } else if (even == 0) return 2;
            else return 3;

        }
        public static void main(String[] args){
            Scanner sc=new Scanner(System.in);
            System.out.println("Enter the length of the array");
            int n=sc.nextInt();
            int[] arr =new int[n];
            for (int i=0;i<n;i++){
                System.out.println("enter the array element");
                arr[i]=sc.nextInt();
            }
            int array_type=arrayType(arr);
            if (array_type==1) System.out.println("Even");
            else if(array_type==2) System.out.println("Odd");
            else System.out.println("Mixed");
        }
    }

